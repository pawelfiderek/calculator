﻿using System;

namespace calculator
{
    class Program
    {
        static Engine en = new Engine();
        static void Main(string[] args)
        {
            Menu();
        }

        static void Menu()
        {
            while(true)
            {
                Console.WriteLine("Wybierz dzialanie: ");

                Console.WriteLine("a. dodawanie");
                Console.WriteLine("b. odejmowanie");
                Console.WriteLine("c. mnozenie");
                string dzialanie = Console.ReadLine();
                Console.WriteLine("podaj wartosc a: ");
                string a = Console.ReadLine();
                Console.WriteLine("podaj wartosc b:");
                string b = Console.ReadLine();
                switch(dzialanie)
                {
                    case "a": Console.WriteLine("wynik to: " + en.Add(int.Parse(a),int.Parse(b)));break;
                    case "b": Console.WriteLine("wynik to: " + en.Minus(int.Parse(a),int.Parse(b)));break;
                    case "c": Console.WriteLine("wynik to: "+ en.Multiply(int.Parse(a),int.Parse(b)));break;
                }
            }
        }
    }
}
