job ('msbuild')
{
    scm {
            git('https://gitlab.com/pawelfiderek/calculator.git') { node ->
                node / gitConfigName('DSL script')
                node / gitConfigEmail('jenkins-dsl-script@altkom.com')
            }
    }

    triggers {
        scm('H/5 * * * *')
    }


    steps {
        msbuild {
            msBuildFile('calculator.csproj')
            msBuildName('some name')
            cmdLineArgs('check')
            buildVariablesAsProperties(false)
            continueOnBuildFailure(false)
            unstableIfWarnings(false)
            doNotUseChcpCommand(false)
        }
    }
}
