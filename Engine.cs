using System;

namespace calculator
{
    class Engine
    {
        public int Add (int a, int b)
        {
            return a+b;
        }

        public int Minus(int a, int b)
        {
            return a-b;
        }

        public int Multiply(int a, int b)
        {
            return a*b;
        }
    }
}
